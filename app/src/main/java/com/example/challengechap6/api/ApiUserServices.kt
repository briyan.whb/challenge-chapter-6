package com.example.challengechap6.api

import com.example.challengechap6.model.GetAllUsersResponseItem
import com.example.challengechap6.model.PostNewUser
import com.example.challengechap6.model.RequestUser
import retrofit2.Call
import retrofit2.http.*

interface ApiUserServices {
    @GET("users")
    fun getAllUsers() : Call<List<GetAllUsersResponseItem>>
    @POST("users")
    fun postDataUser(@Body reqUser : RequestUser) : Call<PostNewUser>
    @PUT("users/{id}")
    fun updateUser(
        @Path("id") id : String,
        @Body request : RequestUser
    ) : Call<RequestUser>
}