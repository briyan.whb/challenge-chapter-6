package com.example.challengechap6.api

import com.example.challengechap6.model.GetMovie
import com.example.challengechap6.model.MovieDetail
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TMDBService {
    @GET("movie/popular")
    fun getAllMovie(
        @Query("api_key") key : String)
    : Call<GetMovie>

    @GET("movie/{movie_id}")
    fun getMovieDetail(
        @Path("movie_id") movieId : Int,
        @Query("api_key") key : String)
    : Call<MovieDetail>
}