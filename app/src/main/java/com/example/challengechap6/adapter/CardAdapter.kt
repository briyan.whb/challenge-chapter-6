package com.example.challengechap6.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.challengechap6.R
import com.example.challengechap6.databinding.FragmentCardBinding
import com.example.challengechap6.model.Result
import com.google.android.material.progressindicator.CircularProgressIndicator

class CardAdapter(private val onClickListener : OnClickListener)
    : RecyclerView.Adapter<CardAdapter.MyViewHolder>() {

    private var _binding : FragmentCardBinding? = null
    private val binding get() = _binding!!

    private val difCallback = object : DiffUtil.ItemCallback<Result>(){
        override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem == newItem
        }
        override fun areContentsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem == newItem
        }
    }

    private val differ = AsyncListDiffer(this,difCallback)

    fun updateData(results : List<Result>) = differ.submitList(results)

    inner class MyViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        var poster : ImageView = binding.image
        var indicator : CircularProgressIndicator = binding.indicator
        var progress : TextView = binding.progressBar
        var judul : TextView = binding.tvTittle
        var date : TextView =  binding.tvDate
        var layout : ConstraintLayout = binding.constraint
        var background = RequestOptions().placeholder(R.color.black)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        _binding = FragmentCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(binding.root)
    }

    override fun getItemCount(): Int = differ.currentList.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentList = differ.currentList[position]

        holder.judul.text = currentList.title
        holder.date.text = currentList.releaseDate
        holder.progress.text = "${(currentList.voteAverage * 10).toInt()}%"
        holder.indicator.progress = (currentList.voteAverage*10).toInt()
        Glide.with(holder.itemView.context)
            .load("https://www.themoviedb.org/t/p/w220_and_h330_face/${currentList.posterPath}")
            .apply(holder.background).into(holder.poster)
        holder.layout.setOnClickListener {
            onClickListener.onClickItem(currentList)
        }
    }

    interface OnClickListener {
        fun onClickItem(data : Result)
    }
}
