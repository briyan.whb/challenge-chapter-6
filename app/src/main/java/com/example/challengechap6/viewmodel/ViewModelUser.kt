package com.example.challengechap6.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.challengechap6.api.ApiUserClient
import com.example.challengechap6.model.GetAllUsersResponseItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ViewModelUser : ViewModel() {
    var liveDataUser : MutableLiveData<List<GetAllUsersResponseItem>> = MutableLiveData()
    fun getLiveUserObserver() : MutableLiveData<List<GetAllUsersResponseItem>> {
        return liveDataUser
    }

    fun setLiveDataUserFromApi(){
        ApiUserClient.instance.getAllUsers()
            .enqueue(object : Callback<List<GetAllUsersResponseItem>> {
                override fun onResponse(
                    call: Call<List<GetAllUsersResponseItem>>,
                    response: Response<List<GetAllUsersResponseItem>>
                ) {
                    if(response.isSuccessful){
                        liveDataUser.postValue(response.body())
                    }else{
                        liveDataUser.postValue(null)
                    }
                }
                override fun onFailure(call: Call<List<GetAllUsersResponseItem>>, t: Throwable) {
                    liveDataUser.postValue(null)
                }

            })
    }
}