package com.example.challengechap6.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.challengechap6.api.TMDBService
import com.example.challengechap6.model.GetMovie
import com.example.challengechap6.model.MovieDetail
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoviesViewModel(private val apiTMDBService: TMDBService) :ViewModel() {

    private val _dataMovies = MutableLiveData<GetMovie>()
    val dataMovies : LiveData<GetMovie> get() = _dataMovies
    private val _dataMovieDetail = MutableLiveData<MovieDetail>()
    val dataMovieDetail : LiveData<MovieDetail> get() = _dataMovieDetail

    fun getAllMovies(){
        apiTMDBService.getAllMovie("fe6de6cd97d185821336fa6719a5c9d3").enqueue(object:
            Callback<GetMovie> {
            override fun onResponse(call: Call<GetMovie>, response: Response<GetMovie>) {
                if(response.isSuccessful){
                    if(response.body() != null){
                        response.body().let {
                            _dataMovies.postValue(response.body())
                        }
                    }
                }
            }
            override fun onFailure(call: Call<GetMovie>, t: Throwable) {

            }
        })
    }

    fun getMovieDetail(id:Int){
        apiTMDBService.getMovieDetail(id,"fe6de6cd97d185821336fa6719a5c9d3").enqueue(object: Callback<MovieDetail>{
            override fun onResponse(call: Call<MovieDetail>, response: Response<MovieDetail>) {
                if(response.isSuccessful){
                    if(response.body()!=null){
                        response.body().let {
                            _dataMovieDetail.postValue(response.body())
                        }
                    }
                }
            }

            override fun onFailure(call: Call<MovieDetail>, t: Throwable) {

            }
        })
    }
}