package com.example.challengechap6.view

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.challengechap6.api.TMDBClient
import com.example.challengechap6.api.TMDBService
import com.example.challengechap6.databinding.FragmentDetailBinding
import com.example.challengechap6.model.MovieDetail
import com.example.challengechap6.viewmodel.MoviesViewModel
import com.example.challengechap6.viewmodel.viewModelsFactory

class Detail : Fragment() {
    private var _binding : FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private val apiTMDBService : TMDBService by lazy { TMDBClient.instance }
    private val moviesViewModel : MoviesViewModel by viewModelsFactory { MoviesViewModel(apiTMDBService) }
    private val args by navArgs<DetailArgs>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        moviesViewModel.getMovieDetail(args.data.id)

        getDetailFromNetwork()
    }

    private fun getDetailFromNetwork(){
        moviesViewModel.dataMovieDetail.observe(viewLifecycleOwner){
            addData(it)
            binding.pbLoading.isVisible = false
        }
    }

    private fun addData(detail: MovieDetail){
        var text1 = ""

        Glide.with(requireContext()).load("https://www.themoviedb.org/t/p/w220_and_h330_face/${detail.posterPath}")
            .into(binding.poster)
        binding.judul.text = detail.title
        binding.dateYear.text = "(${ detail.releaseDate.take(4) })"
        binding.tvDate.text = detail.releaseDate
        for(i in 0 until detail.genres.size){
            text1 = TextUtils.concat(text1, "${detail.genres[i].name}, ").toString()
        }
        binding.genre.text = text1
        binding.pbLoading.progress = (detail.voteAverage*10).toInt()
        binding.tvDetail.text = "${(detail.voteAverage*10).toInt()}%"
        val jam = detail.runtime/60
        val menit = detail.runtime%60
        binding.tvRuntime.text = "${jam}h ${menit}m"
        binding.detail.text = detail.overview
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}