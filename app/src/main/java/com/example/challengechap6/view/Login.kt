package com.example.challengechap6.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.example.challengechap6.R
import com.example.challengechap6.databinding.FragmentLoginBinding
import com.example.challengechap6.datastore.UserManager
import com.example.challengechap6.model.GetAllUsersResponseItem
import com.example.challengechap6.viewmodel.ViewModelUser
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class Login : Fragment() {
    private var _binding : FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel : ViewModelUser
    private lateinit var userManager : UserManager
    private lateinit var listUser : List<GetAllUsersResponseItem>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.login.setOnClickListener {
            getDataUserUsingViewModel(view)
        }

        binding.register.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_login_to_register)
        }
    }

    private fun getDataUserUsingViewModel(view: View) {
        viewModel = ViewModelProvider(this)[ViewModelUser::class.java]
        viewModel.getLiveUserObserver().observe(viewLifecycleOwner) {
            listUser = it
            loginAuth(listUser, view)
        }
        viewModel.setLiveDataUserFromApi()
    }

    private fun loginAuth(user: List<GetAllUsersResponseItem>, view: View) {
        userManager = UserManager(requireContext())
        val inputEmail = binding.edEmail.text.toString()
        val inputPassword = binding.edPass.text.toString()

        if(inputEmail.isNotEmpty() && inputPassword.isNotEmpty()){
            for(i in user.indices){
                if(inputEmail == user[i].email && inputPassword == user[i].password){
                    //if logging in succeded, save the user data
                    Toast.makeText(requireContext(), "Berhasil login", Toast.LENGTH_SHORT).show()
                    GlobalScope.launch {
                        userManager.setBoolean(true)
                        userManager.saveDataLogin(
                            user[i].alamat,
                            user[i].email,
                            user[i].id,
                            user[i].name,
                            user[i].password,
                            user[i].tanggal,
                            user[i].username
                        )
                    }
                    Navigation.findNavController(view).navigate(R.id.action_login_to_home2)
                }
            }
        }else{
            Toast.makeText(requireContext(), "Semua field harus diisi", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}