package com.example.challengechap6.view

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.asLiveData
import androidx.navigation.Navigation
import com.example.challengechap6.R
import com.example.challengechap6.api.ApiUserClient
import com.example.challengechap6.databinding.FragmentProfileBinding
import com.example.challengechap6.datastore.UserManager
import com.example.challengechap6.model.RequestUser
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Profile : Fragment() {
    private var _binding : FragmentProfileBinding? = null
    private val binding get() = _binding!!
    private lateinit var userManager: UserManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userManager = UserManager(requireContext())

        initField()
        binding.logout.setOnClickListener {
            logout(view)
        }
        binding.update.setOnClickListener {
            updateData(view)
        }
    }

    private fun initField() {
        userManager.name.asLiveData().observe(viewLifecycleOwner){
            binding.edNama.setText(it.toString())
        }
        userManager.dateOfBirth.asLiveData().observe(viewLifecycleOwner){
            binding.edTanggal.setText(it.toString())
        }
        userManager.address.asLiveData().observe(viewLifecycleOwner){
            binding.edAlamat.setText(it.toString())
        }
        userManager.email.asLiveData().observe(viewLifecycleOwner){
            binding.edEmail.setText(it.toString())
        }
        userManager.username.asLiveData().observe(viewLifecycleOwner){
            binding.edUser.setText(it.toString())
        }
        userManager.password.asLiveData().observe(viewLifecycleOwner){
            binding.edPass.setText(it.toString())
            binding.edPassX.setText(it.toString())
        }
    }

    private fun updateData(view: View) {
        var id = ""
        val alamat = binding.edAlamat.text.toString()
        val email = binding.edEmail.text.toString()
        val username = binding.edUser.text.toString()
        val tanggal = binding.edTanggal.text.toString()
        val password = binding.edPass.text.toString()
        val nama = binding.edNama.text.toString()

        userManager.IDuser.asLiveData().observe(viewLifecycleOwner){
            id = it.toString()
        }
        AlertDialog.Builder(requireContext())
            .setTitle("Update data")
            .setMessage("Yakin ingin mengupdate data?")
            .setNegativeButton("TIDAK"){ dialogInterface : DialogInterface, _: Int ->
                dialogInterface.dismiss()
            }
            .setPositiveButton("YA"){ _: DialogInterface, _: Int ->
                ApiUserClient.instance.updateUser(id, RequestUser(
                    alamat,
                    email,
                    nama,
                    password,
                    tanggal,
                    username
                )).enqueue(object : Callback<RequestUser> {
                    override fun onResponse(
                        call: Call<RequestUser>,
                        response: Response<RequestUser>
                    ) {
                        GlobalScope.launch {
                            userManager.saveDataLogin(
                                alamat,
                                email,
                                id,
                                nama,
                                password,
                                tanggal,
                                username
                            )
                        }
                        Toast.makeText(requireContext(), "Data Telah Diupdate", Toast.LENGTH_LONG).show()
                        Navigation.findNavController(view).navigateUp()
                    }
                    override fun onFailure(call: Call<RequestUser>, t: Throwable) {
                    }

                })
            }.show()
    }

    private fun logout(view: View) {
        AlertDialog.Builder(requireContext())
            .setTitle("Logout")
            .setMessage("Apakah anda yakin ingin logout?")
            .setNegativeButton("TIDAK"){ dialogInterface : DialogInterface, _: Int ->
                dialogInterface.dismiss()
            }
            .setPositiveButton("YA"){ _: DialogInterface, _: Int ->
                GlobalScope.launch {
                    userManager.clearDataLogin()
                }
                Toast.makeText(requireContext(), "Anda Telah Logout", Toast.LENGTH_LONG).show()
                Navigation.findNavController(view).navigate(R.id.action_profile_to_login)
            }.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}