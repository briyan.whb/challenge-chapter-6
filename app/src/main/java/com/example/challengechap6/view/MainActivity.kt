package com.example.challengechap6.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.challengechap6.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_main)
    }
}