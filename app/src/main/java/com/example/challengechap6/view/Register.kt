package com.example.challengechap6.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.challengechap6.api.ApiUserClient
import com.example.challengechap6.databinding.FragmentRegisterBinding
import com.example.challengechap6.model.PostNewUser
import com.example.challengechap6.model.RequestUser
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Register : Fragment() {
    private var _binding : FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentRegisterBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.confirm.setOnClickListener {
            val nama: String = binding.edNama.text.toString()
            val alamat: String = binding.edAlamat.text.toString()
            val tanggal: String = binding.edTanggalLahir.text.toString()
            val user: String = binding.edUser.text.toString()
            val email: String = binding.edEmail.text.toString()
            val pass: String = binding.edPass.text.toString()
            val passX: String = binding.edPassX.text.toString()

            if (nama.isNotEmpty() && tanggal.isNotEmpty() && alamat.isNotEmpty() &&
                user.isNotEmpty() && pass.isNotEmpty() && email.isNotEmpty() &&
                passX.isNotEmpty()
            ) {
                //check similarity of password and konfirmasiPassword
                if (pass == passX) {
                    postDataNewUser(alamat, email, user, view, tanggal, pass, nama)
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Password dan konfirmasi password harus sama",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                Toast.makeText(requireContext(), "Semua field harus diisi", Toast.LENGTH_SHORT)
                    .show()
            }

        }
    }

    private fun postDataNewUser(
        alamat: String, email: String, username: String, view: View,
        tanggalLahir: String, password: String, name: String
    ) { ApiUserClient.instance.postDataUser(
        RequestUser(
            alamat,
            email,
            name,
            password,
            tanggalLahir,
            username
        )
    ).enqueue(object : Callback<PostNewUser> {
        override fun onResponse(call: Call<PostNewUser>, response: Response<PostNewUser>) {
            if (response.isSuccessful) {
                checkIfFragmentAttached {
                    Toast.makeText(
                        requireContext(),
                        "Registrasi berhasil",
                        Toast.LENGTH_SHORT
                    ).show()
                    Navigation.findNavController(view).navigateUp()
                }

            } else {
                checkIfFragmentAttached {
                    Toast.makeText(requireContext(), response.message(), Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
        override fun onFailure(call: Call<PostNewUser>, t: Throwable) {
            checkIfFragmentAttached {
                Toast.makeText(requireContext(), t.message, Toast.LENGTH_SHORT).show()
            }
        }
    })
    }
    fun checkIfFragmentAttached(operation: Context.() -> Unit) {
        if (isAdded && context != null) {
            operation(requireContext())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}