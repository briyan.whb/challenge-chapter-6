package com.example.challengechap6.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.asLiveData
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.example.challengechap6.R
import com.example.challengechap6.adapter.CardAdapter
import com.example.challengechap6.api.TMDBClient
import com.example.challengechap6.api.TMDBService
import com.example.challengechap6.databinding.FragmentHomeBinding
import com.example.challengechap6.datastore.UserManager
import com.example.challengechap6.model.Result
import com.example.challengechap6.viewmodel.MoviesViewModel
import com.example.challengechap6.viewmodel.viewModelsFactory

class Home : Fragment() {
    private var _binding : FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private lateinit var moviesAdapter : CardAdapter
    private lateinit var userManager: UserManager
    private val apiTMDBService : TMDBService by lazy { TMDBClient.instance }
    private val viewModel : MoviesViewModel by viewModelsFactory { MoviesViewModel(apiTMDBService) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userManager = UserManager(requireContext())
        userManager.username.asLiveData().observe(viewLifecycleOwner) {
            binding.textView2.text = "Welcome $it"
        }

        initRecycler(view)
        viewModel.getAllMovies()
        getDataFromNetwork()
        binding.profile.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_home2_to_profile)
        }
    }

    private fun initRecycler(view: View){
        moviesAdapter = CardAdapter(object : CardAdapter.OnClickListener {
            override fun onClickItem(data: Result) {
                val action = HomeDirections.actionHome2ToDetail(data)
                Navigation.findNavController(view).navigate(action)
            }
        })
        binding.apply {
            recyclerView.adapter = moviesAdapter
            recyclerView.layoutManager = GridLayoutManager(requireContext(),2)
        }
    }

    private fun getDataFromNetwork(){
        viewModel.dataMovies.observe(viewLifecycleOwner){
            moviesAdapter.updateData(it.results)
            binding.loading.isVisible = false
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}